package Ro.Orange;

public abstract class Person {
    String name;
    String gender;

    public Person(String name, String gender) {
        this.name = name;
        this.gender = gender;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    abstract String work();

    public String toString(){
        return "Name = " + this.name + ",Gender = " +this.gender;
    }

    public void changeName (String newName, String newGender) {
        this.name= newName;
        this.gender = newGender;
    }
}
