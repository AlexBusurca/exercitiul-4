package Ro.Orange;

public class HotelEmployee extends Person {
    private int empId;

    public HotelEmployee (String name, String gender, int empId) {
        super (name, gender);
        this.empId = empId;
    }

    @Override
    String work() {
        if (empId!=0) return "Working as hotel employee!!!";
        else return "";
    }
}
